import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import {fetchMovies} from "../../actions/movie/movies.action";
import {connect} from "react-redux";
import {Body, Button, Header, Icon, Left, Right, Text, Title} from "native-base";
import HomeContent from "./HomeContent";

class HomePage extends Component {
    componentWillMount() {
        this.props.fetchMovies();
    }

    render() {
        return (
            <Fragment>
                <Header style={{backgroundColor: '#17191D'}} androidStatusBarColor={"#17191D"}>
                    <Body>
                    <Title>Popkorn</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='search'/>
                        </Button>
                        <Button transparent>
                            <Icon name='settings'/>
                        </Button>
                    </Right>
                </Header>
                {/*<Text style={{*/}
                    {/*backgroundColor: '#17191D',*/}
                    {/*color : 'white',*/}
                    {/*fontSize : 30,*/}
                    {/*marginLeft : 10,*/}
                    {/*marginTop : 10,*/}
                    {/*marginBottom : 10,*/}
                    {/*fontWeight : 'bold'*/}
                {/*}}>Movies</Text>*/}
                {
                    this.props.movies.fetching ?
                        <Text style={{ color :'white' }}>Loading</Text> :
                        <HomeContent movies={this.props.movies} style={{ flex : 1 }}/>
                }
            </Fragment>
        );
    }
}

HomePage.propTypes = {};

const mapStateToProps = ({movies}) => {
    return {
        movies
    }
};

const mapDispatchToProps = {
    fetchMovies
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
