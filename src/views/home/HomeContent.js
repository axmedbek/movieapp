import React from 'react';
import {FlatList, ScrollView, StyleSheet} from 'react-native';
import {Content, ScrollableTab, Tab, Tabs, Text, View} from "native-base";
import {Col, Row, Grid} from 'react-native-easy-grid';
import CartItem from "../../components/CartItem";

const styles = StyleSheet.create({
    tabStyle: {
        backgroundColor: '#17191D'
    }
});
const HomeContent = ({ movies }) => {
    console.log(movies);
    return (
        <Tabs renderTabBar={() => <ScrollableTab/>}>
            <Tab heading="Now playing" tabStyle={{backgroundColor: '#17191D'}}
                 activeTabStyle={{backgroundColor: '#17191D'}} style={styles.tabStyle}>
                <Content>
                    <FlatList
                        data={movies.total_data.items}
                        renderItem={({ item,index }) =>
                            <CartItem movie={item} style={ movies.total_data.items.length === index ? {marginBottom : 20} : {}}/>
                        }
                        numColumns={2}
                        style={{ marginLeft : 5}}
                    />
                </Content>
            </Tab>
            <Tab heading="Popular" tabStyle={{backgroundColor: '#17191D'}} activeTabStyle={{backgroundColor: '#17191D'}}
                 style={styles.tabStyle}>
                <Text>t1</Text>
            </Tab>
            <Tab heading="Top rated" tabStyle={{backgroundColor: '#17191D'}}
                 activeTabStyle={{backgroundColor: '#17191D'}} style={styles.tabStyle}>
                <Text>t1</Text>
            </Tab>
            <Tab heading="Upcoming" tabStyle={{backgroundColor: '#17191D'}}
                 activeTabStyle={{backgroundColor: '#17191D'}} style={styles.tabStyle}>
                <Text>t1</Text>
            </Tab>
        </Tabs>
    );
};

export default HomeContent;
