import {FETCH_MOVIES} from "./movies.action_types";
import axios from 'axios';

export function fetchMovies(){
    return dispatch => {
        dispatch({
           type : FETCH_MOVIES,
           payload : axios.get('https://api.themoviedb.org/3/list/2?api_key=4a06278524786329fb6125f3a71239f9')
               .then(result => result.data)
               .catch(error => console.log(error))
        });
    }
}