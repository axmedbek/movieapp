import React from 'react';
import {Col, Grid, Icon, Row, View} from "native-base";

const RatingComponent = ({rating}) => {
    const count = rating / 4;
    console.log(count);
    return (
        <Grid>
            <Row>
                {
                    [1, 2, 3, 4, 5].map(item => (
                        <Col>
                            {
                                item < count
                                    ? <Icon name={"star"} style={{color: '#F9D235',fontSize : 15,marginLeft : 5}}/>
                                    : <Icon name={"star-outline"} style={{color: '#F9D235',fontSize : 15,marginLeft : 5}}/>
                            }
                        </Col>
                    ))
                }
            </Row>

        </Grid>
    );
};

export default RatingComponent;
