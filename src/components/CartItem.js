import React from 'react';
import {Grid, Icon, Text, View} from "native-base";
import {Image} from "react-native";
import RatingComponent from "./RatingComponent";

const CartItem = ({ movie }) => {
    return (
        <View style={{ height : 250,width : '45%', marginLeft : 10 , marginTop : 5 }}>
            <Image source={{ uri : `https://image.tmdb.org/t/p/w200${movie.poster_path}` }}
                   style={{ height : 200,borderRadius : 5}}/>
            <Text style={{ fontWeight : 'bold',color : 'white'}}>{movie.original_title}</Text>
            <Grid>
                <Text style={{ color : 'white',fontSize : 12}}>{movie.release_date}</Text>
                <RatingComponent rating={movie.popularity}/>
                {/*<Text><Icon name={"star"} style={{ color :'#F9D235'}}/></Text>*/}
            </Grid>
        </View>
    );
};

export default CartItem;
