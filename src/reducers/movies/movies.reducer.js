import {
    FETCH_MOVIES_FULFILLED,
    FETCH_MOVIES_PENDING,
    FETCH_MOVIES_REJECTED
} from "../../actions/movie/movies.action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_MOVIES_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_MOVIES_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_MOVIES_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;
