import {combineReducers} from "redux";
import { fetch_movies_reducer } from "./movies";

export default combineReducers({
    movies : fetch_movies_reducer
});