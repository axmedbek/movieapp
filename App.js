import React, {Component} from 'react';
import {Provider} from "react-redux";
import {createStore,applyMiddleware} from "redux";
import rootReducer from "./src/reducers/rootReducer";
import thunk from 'redux-thunk';
import reduxPromise from 'redux-promise-middleware';
import HomePage from "./src/views/home/HomePage";
import logger from 'redux-logger';
import {Container} from "native-base";

const store = createStore(
    rootReducer,
    applyMiddleware(reduxPromise(),thunk,logger)
);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Container style={{ backgroundColor : '#17191D'}}>
                    <HomePage/>
                </Container>
            </Provider>
        );
    }
}


export default App;
